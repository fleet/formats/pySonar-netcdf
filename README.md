# PySonar Netcdf

Python code repository related to Sonar Netcdf (https://github.com/ices-publications/SONAR-netCDF4) format and its bathymetry extension XSF format

## How to use

. Install sonar_netcdf in your environment with PIP

```
pip install sonar_netcdf --extra-index-url https://gitlab.ifremer.fr/api/v4/projects/1480/packages/pypi/simple
```

. Now you can use it
```
from sonar_netcdf.utimls import nc_reader as sn
reader = sn.NcReader(myfile)
....

```



### Contribute : Get Started!

Ready to contribute? Here's how to set up `pySonar-netcdf` for local development.

. Install anaconda or miniconda on your computer
. Clone the `pySonar-netcdf` repo on GitLab.

. Create your developpent environnement with the create_anaconda_environments.py script in requirements directory.
+
`cd requirements`
+
`python create_anaconda_environments.py`
+

. activate anaconda environment
`conda activate sonar_netcdf`


### Deploy new release

pySonar-netcdf project can be pip installable. Use the following commands :

```
python -m build
python -m twine upload --repository gitlab-sonar-netcdf dist/*
```

Note : id of server is defined in ~/.pypirc.

