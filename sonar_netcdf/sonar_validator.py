import inspect
import re

import netCDF4 as nc

from . import sonar_validator_model as nc_model

# pylint issue with relative import https://github.com/PyCQA/pylint/issues/3651
# pylint: disable=import-error
from .sonar_validator_model import mandatory, mandatory_if_applicable, recommended, optional
from .utils import print_color as p
from .utils.nc_encoding import open_nc_file

# pylint: enable=import-error


class SonarChecker:
    def __init__(self):
        # parse static model files
        self.classes = {}
        for name, obj in inspect.getmembers(nc_model):
            if inspect.isclass(obj):
                self.classes[name] = obj
        self.current_file_path = ""
        self._reset_stats()

    def _head(self, msg):
        p.header(msg)

    def _info(self, msg):
        p.info(msg)

    def _warning(self, msg):
        self.warning_count += 1
        p.warning(msg)
        # logging.warning(msg)

    def _error(self, msg):
        self.error_count += 1
        p.error(msg)
        # logging.error(msg)

    def _get_validator_class(self, path: str):
        """Return the associated test class in sonar_validator_model for this group, or None if not found"""
        if not path.endswith("/"):
            path += "/"

        for value in self.classes.values():
            pattern = value.get_group_path_pattern()
            if re.fullmatch(pattern, path):
                return value
        return None

    def validate_grp(self, dataset: nc.Dataset):
        self._info(f"Checking group {dataset.path}")
        validator_class = self._get_validator_class(dataset.path)
        if validator_class is None:
            self._warning(f"Cannot match any known group definition for {dataset.path}")
            return

        for expected_type in validator_class._enum_types:
            if expected_type not in dataset.enumtypes:
                self._error(f"Missing enum declaration {expected_type} expected in {dataset.path}")

        for expected_type in validator_class._vlen_types:
            if expected_type not in dataset.vltypes:
                self._error(f"Missing vlen type declaration {expected_type} expected in {dataset.path}")

        # compound types not done
        # enum

        # Dimensions
        # first validate dimensions, check that all dimension are defined in source dataset
        for expected_dimension in validator_class._dimensions:
            if expected_dimension not in dataset.dimensions:
                self._error(f"Missing dimension {expected_dimension} in {dataset.path}")

        # Attributes
        for expected, obligation in validator_class._attributes.items():
            missing = expected not in dataset.ncattrs()
            if missing and obligation == mandatory:
                self._error(f"Missing mandatory group attribute {expected} in {dataset.path}")
            if missing and obligation == recommended:
                self._warning(f"Missing recommended group attribute {expected} in {dataset.path}")
            if missing and obligation == mandatory_if_applicable:
                self._info(
                    f"To be manually checked : missing mandatory_if_applicable group attribute {expected} in {dataset.path}"
                )
            if missing and obligation == optional:
                self._info(f"Missing optional group attribute {expected} in {dataset.path}")

        # Coordinate Variables
        for expected, obligation in validator_class._coordinate_variables.items():
            missing = expected not in dataset.variables
            if missing and obligation == mandatory:
                self._error(f"Missing mandatory group Coordinate Variable {expected} in {dataset.path}")
            if missing and obligation == recommended:
                self._warning(f"Missing recommended group Coordinate Variable {expected} in {dataset.path}")
            if missing and obligation == mandatory_if_applicable:
                self._info(
                    f"To be manually checked : missing mandatory_if_applicable group Coordinate Variable  {expected} in {dataset.path}"
                )
            if missing and obligation == optional:
                self._info(f"Missing optional group Coordinate Variable {expected} in {dataset.path}")

        # Variables
        for expected, obligation in validator_class._variables.items():
            missing = expected not in dataset.variables
            if missing and obligation == mandatory:
                self._error(f"Missing mandatory variable {expected} in {dataset.path}")
            if missing and obligation == recommended:
                self._warning(f"Missing recommended variable {expected} in {dataset.path}")
            if missing and obligation == mandatory_if_applicable:
                self._info(
                    f"To be manually checked : missing mandatory_if_applicable variable {expected} in {dataset.path}"
                )
            if missing and obligation == optional:
                self._info(f"Missing optional variable {expected} in {dataset.path}")

        # we do not do any check on mandatory_if_applicable variables or optional variable

        # recurse subgroups
        for grp in dataset.groups:
            self.validate_grp(dataset[grp])

    def validate_file(self, file_path: str):
        self._reset_stats()
        self.current_file_path = file_path
        self._head(f"Checking file f{file_path}")
        # open the file
        with open_nc_file(file_path, mode="r") as file:
            # and validate each group
            self.validate_grp(file)
        self._print_result()

    def _reset_stats(self):
        self.error_count = 0
        self.warning_count = 0

    def _print_result(self):
        self._head(f"{self.current_file_path} : {self.error_count} errors, {self.warning_count} warnings")


if __name__ == "__main__":
    # "c:/data/datasets/netcdfInspector/GLOBE_MERGED_GAZCOGNE3_003_20180829_061940_1.xsf_TO_GAZCOGNE3_003_20180829_070941_1.xsf_v783u7_w_ES333-7C-333000.nc"

    # This should be run as a module to work correctly
    # file = r"C:\data\datasets\NetCDF_RUN003/PELGAS-2022_003_20220428_103054_1.nc"
    # checker = SonarChecker()
    # checker.validate_file(file)

    import glob
    import os

    for f in glob.glob(r"C:\Users\lberger\Desktop\ESSDRIX21\Gridded_Europe\*.nc"):
        checker = SonarChecker()
        checker.validate_file(f)
