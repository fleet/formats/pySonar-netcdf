import os
import tempfile as tmp

import netCDF4 as nc
import numpy as np

# pylint issue with relative import ?
# pylint:disable = import-error
from ..sonar_groups import RootGrp, EnvironmentGrp, SonarGrp, BeamGroup1Grp

# pylint:enable = import-error


def create_basic(file: nc.Dataset):
    """create basic structure with a few fields for test"""
    # create root Node

    root_structure = RootGrp()
    root = root_structure.create_group(file)

    # create environment
    env_structure = EnvironmentGrp()
    env = env_structure.create_group(root)
    env_structure.create_dimension(env, {EnvironmentGrp.FREQUENCY_DIM_NAME: 2})

    variable = env_structure.create_frequency(env)
    variable = env_structure.create_sound_speed_indicative(env)
    # create data
    variable[:] = np.float32(1520.3)

    sonar_structure = SonarGrp()
    sonar = sonar_structure.create_group(root)
    beam_structure = BeamGroup1Grp()
    for i in range(0, 1):
        beam = beam_structure.create_group(sonar, ident=f"Beam_group{i}")

        beam_structure.create_dimension(
            beam,
            {
                BeamGroup1Grp.BEAM_DIM_NAME: 20,
                BeamGroup1Grp.SUBBEAM_DIM_NAME: 1,
                BeamGroup1Grp.TX_BEAM_DIM_NAME: 1,
                BeamGroup1Grp.PING_TIME_DIM_NAME: 100,
            },
        )
        beam_structure.create_beam_type(beam)


def test_add_attribute():
    """Add some attribute to a variable and group"""
    filename = tmp.mktemp(suffix=".nc")
    print(f"creating fake xsf file {filename}")
    with nc.Dataset(filename, mode="w") as file:
        root_structure = RootGrp()
        root = root_structure.create_group(file, vlen_type_dict={}, att_1="text", att_2=1.5)
        assert root.att_1 == "text"
        assert root.att_2 == 1.5
        assert isinstance(root.att_2, float)


def test_change_type():
    """Add change a vlen type and add some attribute to a variable"""
    filename = tmp.mktemp(suffix=".nc")
    # filename = tmp.mktemp(suffix=".nc")
    print(f"creating fake xsf file {filename}")

    with nc.Dataset(filename, mode="w") as file:
        root_structure = RootGrp()
        root = root_structure.create_group(file)
        sonar_structure = SonarGrp()
        # create sonar group, change sample_t type and it missing value
        sonar = sonar_structure.create_group(root)
        beam_structure = BeamGroup1Grp()
        beam = beam_structure.create_group(
            sonar, vlen_type_dict={"sample_t": np.float32, "angle_t": np.int16}, ident="Beam_group0"
        )
        beam_structure.create_dimension(
            beam,
            {
                BeamGroup1Grp.BEAM_DIM_NAME: 20,
                BeamGroup1Grp.SUBBEAM_DIM_NAME: 1,
                BeamGroup1Grp.TX_BEAM_DIM_NAME: 1,
                BeamGroup1Grp.PING_TIME_DIM_NAME: 100,
            },
        )
        angle = beam_structure.create_echoangle_major(
            beam,
            missing_value=-32768,  # missing value is not in CF convention and use of _FillValue not supported by netcdf API
            valid_range=np.int16([-18000, 18000]),
            scale_factor=np.float32(0.01),
        )
        assert np.all(angle.valid_range == np.int16([-18000, 18000]))
        assert angle.scale_factor == np.float32(0.01)
        assert angle.missing_value == -32768

    os.remove(filename)


def test_append_ping():
    filename = tmp.mktemp(suffix=".nc")
    print(f"creating fake xsf file {filename}")

    with nc.Dataset(filename, mode="w") as file:
        root_structure = RootGrp()
        root = root_structure.create_group(file)
        sonar_structure = SonarGrp()
        sonar = sonar_structure.create_group(root)
        beam_structure = BeamGroup1Grp()
        beam = beam_structure.create_group(sonar, ident="Beam_group0")
        beam_structure.create_dimension(
            beam,
            {
                BeamGroup1Grp.BEAM_DIM_NAME: 2,
                BeamGroup1Grp.SUBBEAM_DIM_NAME: 1,
                BeamGroup1Grp.TX_BEAM_DIM_NAME: 1,
                BeamGroup1Grp.PING_TIME_DIM_NAME: None,
            },
        )
        bs = beam_structure.create_backscatter_r(beam)
        # append two values and check that ping length is equal to 2
        bs[0, 0, 0] = np.array([1, 2, 3], dtype=np.float32)
        bs[0, 1, 0] = np.array([1, 2, 3], dtype=np.float32)
        bs[1, 0, 0] = np.array([1, 2, 3], dtype=np.float32)
        assert len(beam.dimensions[BeamGroup1Grp.PING_TIME_DIM_NAME]) == 2


def test_netcdf_basic():
    """sample test and use of sonar_groups.py file and methods"""
    filename = tmp.mktemp(suffix=".nc")
    print(f"creating fake xsf file {filename}")

    with nc.Dataset(filename, mode="w") as file:
        create_basic(file)

    # if no exception we consider to test valid
    assert True

    os.remove(filename)
