"""
Tests of module sonar_netcdf.sonar_variables
"""

import tempfile as tmp

import netCDF4 as nc
import numpy as np

# pylint issue with relative import https://github.com/PyCQA/pylint/issues/3651
# pylint: disable=import-error
from .. import sonar_groups as sg
from ..utils import nc_reader as nc_r
from ..utils import nc_variables as nc_v

# pylint: enable=import-error


def test_find_time_coordinate_variables():
    """check find_time_coordinate_variables function"""
    with tmp.TemporaryDirectory() as o_dir:
        filename = tmp.mktemp(dir=o_dir, suffix=".nc")

        # Generates a Sonar files with 5 time coordinate variables
        with nc.Dataset(filename, mode="w") as file:
            root_structure = sg.RootGrp()
            root = root_structure.create_group(file)

            # create /annotation
            ano_structure = sg.AnnotationGrp()
            ano = ano_structure.create_group(root)
            ano_structure.create_dimension(ano, {sg.AnnotationGrp.TIME_DIM_NAME: 2})
            ano_structure.create_time(ano, long_name="ANO_TIME")
            ano_structure.create_annotation_text(ano)
            # create /platform
            pla_structure = sg.PlatformGrp()
            pla = pla_structure.create_group(root)
            pla_structure.create_dimension(pla, {"time1": 2})
            var = pla.createVariable(varname="time1", datatype=float, dimensions=("time1"), fill_value=np.nan)
            var.long_name = "PLA_TIME"
            var.units = "nanoseconds since 1970-01-01 00:00:00Z"
            # create /platform/NMEA
            nmea_structure = sg.NmeaGrp()
            nmea = nmea_structure.create_group(pla, ident="NMEA")
            nmea_structure.create_dimension(nmea, {sg.NmeaGrp.TIME_DIM_NAME: 2})
            nmea_structure.create_time(nmea, long_name="NMEA_TIME")
            nmea_structure.create_nmea_datagram(nmea)
            # create /Sonar
            sonar_structure = sg.SonarGrp()
            sonar = sonar_structure.create_group(root)
            beam_structure = sg.BeamGroup1Grp()
            for i in range(2):
                beam = beam_structure.create_group(sonar, ident=f"Beam_group{i}")
                beam_structure.create_dimension(
                    beam, {sg.BeamGroup1Grp.BEAM_DIM_NAME: 20, sg.BeamGroup1Grp.PING_TIME_DIM_NAME: 2}
                )
                beam_structure.create_beam_type(beam)
                beam_structure.create_ping_time(beam, long_name=f"BEAM{i}_TIME")
                beam_structure.create_platform_heading(beam)

        with nc_r.open_nc_reader(filename) as reader:
            all_long_names = [variable.long_name for variable in nc_v.find_time_coordinate_variables(reader)]
            assert len(all_long_names) == 5

            for long_name in all_long_names:
                assert long_name in [
                    "ANO_TIME",
                    "PLA_TIME",
                    "NMEA_TIME",
                    "BEAM0_TIME",
                    "BEAM1_TIME",
                ]
