"""
A list of vendor specific type that could might not be clearly defined in sonar-netcdf model
"""
from enum import Enum


class KmPingMode(Enum):
    """Enum for .all kongsberg ping mode"""

    UNKNOW = -1
    VERY_SHALLOW = 0
    SHALLOW = 1
    MEDIUM = 2
    DEEP = 3
    DEEPER = 4
    VERY_DEEP = 5
    EXTRA_DEEP = 6
    EXTREME_DEEP = 7


class KmPulseLengthMode(Enum):
    """Enum for .all Kongbserg pulse length mode"""

    UNKNOWN = -1
    VERY_SHORT_CW = 0
    SHORT_CW = 1
    MEDIUM_CW = 2
    LONG_CW = 3
    VERY_LONG_CW = 4
    EXTRA_LONG_CW = 5
    SHORT_FM = 6
    LONG_FM = 7
