from contextlib import contextmanager
from typing import Generator, Optional

import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np

from .nc_encoding import open_nc_file

# pylint issue with relative import https://github.com/PyCQA/pylint/issues/3651
# pylint: disable=import-error
# pylint: enable=import-error


# ensure minimum size for figures
plt.rcParams["figure.dpi"] = 270

# pylint:disable=redefined-outer-name


class NcReader:
    """
    Class used for reading a Sonar-netcdf like file
    It has a set of methods allowing to parse and dump the file content on a jupyter netbook
    """

    def __init__(self, filename: str, quiet: bool = False):
        """

        :param filename: the file path to read
        :param quiet: False to enter in verbose mode
        """
        # open the file
        self.file_name = filename
        self.quiet = quiet
        # open the file. May raise an OSError
        self.dataset = open_nc_file(self.file_name)

    def close(self):
        # close the file
        # "dataset" not in self.__dict__ when Netcdf was unable to open the file
        if "dataset" in self.__dict__ and self.dataset is not None:
            self.dataset.close()
            self.dataset = None

    def __del__(self):
        # close the file
        self.close()

    def _get_variable_data(self, variable_path, slice_index=None):
        """
        Return the variable as a np array matrix (either 1D or 2D)
        :param variable_path: path to the variable name
        :param substract_tvg_offset: do we substract TVG offset trying to something close to an absolute level value for backscatter
        :param variable_name: the name of the variable to read
        :param index: the current swath id, for WC data it will allow to load the current swath data only
        :return: a np array
        """
        variable = self.dataset[variable_path]
        if slice_index is None:
            slice_index = {}
        if NcReader.is_variable_vlen(variable):
            return self._get_vlen_variable(variable_path, slice_index)
        return self._get_usual_variable(variable_path=variable_path, slice_index=slice_index)

    def _get_variable(self, variable_path):
        return self.dataset[variable_path]

    def find_variable(self, variable_path) -> Optional[nc.Variable]:
        try:
            return self.dataset[variable_path]
        except IndexError:
            return None

    @staticmethod
    def is_variable_vlen(variable):
        """
        Check if the given variable is a variable length variable (in XSF variable length definition)
        """
        return variable._isvlen

    def _squeeze_shape(self, shape: tuple, dimensions: tuple):
        """
        Remove dimension equals to 1 in a tuple list, if everything is equal to 1 the initia
        :param shape:
        :return: a tuple with 1 value removed
        """
        final_shape = ()
        final_dims = ()

        for sh, dim in zip(shape, dimensions):
            if sh != 1:
                final_shape += (sh,)
                final_dims += (dim,)
        return final_shape, final_dims

    def _apply_scale_offset(self, values: np.ndarray, scale_factor=None, add_offset=None, missing_value=None):
        replace_masked_values = False
        mask = (values == missing_value) if missing_value is not None else None
        if scale_factor is not None:
            values = values * scale_factor
            replace_masked_values = True
        if add_offset is not None:
            values = values + add_offset
        if mask is not None and replace_masked_values:
            values[mask] = np.nan
        return values

    def _get_usual_variable(self, variable_path, slice_index: dict):
        l_variable = self.dataset[variable_path]
        shape = l_variable.shape
        dimensions = l_variable.dimensions
        reduced_shape, reduced_dimensions = self._squeeze_shape(shape, dimensions)

        # we try to reduce the size of the data, to do so we'll use the slice index dictionnary

        values = np.squeeze(l_variable[:])
        # compute slice taking into account for slice_index
        selection_applied = ""
        reduced_shape_in_progress = ()
        reduced_dim_in_progress = ()
        slicing = ()
        for sh, dim in zip(reduced_shape, reduced_dimensions):
            if dim in slice_index:
                index = slice_index[dim]
                slicing += (slice(index, index + 1),)  # we select the slice specified in index
            else:
                reduced_shape_in_progress += (sh,)
                reduced_dim_in_progress += (dim,)
                slicing += (slice(None),)  # we select all data
        values = values[slicing]
        values = np.squeeze(values)
        return values.transpose()

    def _get_vlen_variable(self, variable_path, slice_index: dict):
        """
        retrieve a matrix containing a slice (matrix) of all vlen data, filled with NaN values
        """
        vlen_variable = self.dataset[variable_path]
        shape = vlen_variable.shape
        dimensions = vlen_variable.dimensions
        reduced_shape, reduced_dimensions = self._squeeze_shape(shape, dimensions)
        vlen_variable.set_auto_maskandscale(False)
        # with netcdf < 1.5.4 we need to take into account for scale factor and add offset by ourself
        scale_factor = None
        add_offset = None
        if hasattr(vlen_variable, "scale_factor"):
            scale_factor = vlen_variable.scale_factor
        if hasattr(vlen_variable, "add_offset"):
            add_offset = vlen_variable.add_offset
        missing_value = None
        if hasattr(vlen_variable, "missing_value"):
            missing_value = vlen_variable.missing_value
        if hasattr(vlen_variable, "_FillValue"):
            missing_value = vlen_variable._FillValue

        if len(reduced_shape) == 0:
            # data are like a vector shape
            values = vlen_variable[:]
            values = np.squeeze(values)
            # values = self._apply_scale_offset(scale_factor, add_offset, missing_value)
            return values[:]

        if len(reduced_shape) > 1:
            # data is still with a dimensionnality higher than 1 (in fact 2 with its vlen charactéristic)
            # we need to reduce this, to do so we'll use the slice index dictionnary

            values = np.squeeze(vlen_variable[:])
            # compute slice taking into account for slice_index
            selection_applied = ""
            reduced_shape_in_progress = ()
            reduced_dim_in_progress = ()
            slicing = ()
            for sh, dim in zip(reduced_shape, reduced_dimensions):
                if dim in slice_index:
                    index = slice_index[dim]
                    slicing += (slice(index, index + 1),)  # we select the slice specified in index
                else:
                    reduced_shape_in_progress += (sh,)
                    reduced_dim_in_progress += (dim,)
                    slicing += (slice(None),)  # we select all data
            values = values[slicing]
            values = np.squeeze(values)
            reduced_shape = reduced_shape_in_progress
            reduced_dimensions = reduced_dim_in_progress
        else:
            #            values = vlen_variable[:]
            # values = np.empty(shape=reduced_shape, dtype=object)
            values = np.squeeze(vlen_variable[:])

        if not self.quiet:
            print(
                f"Vlen variable {variable_path} : reduced shape for display is  {reduced_dimensions} ({reduced_shape})"
            )
        if len(reduced_shape) == 1:
            # dimension is 1 like a ping indexed variable, we can parse all vlen data and build a matrix from it
            values = np.squeeze(values)
            # retrieve max size
            max_samples = 0
            for sub_array in values:  # in a performance point of view we'll read the data twice
                max_samples = max(max_samples, len(sub_array))
            # fill ping with data\n",
            matrix = np.full(
                (reduced_shape[0], max_samples),
                dtype="float32",
                fill_value=float(np.nan),
            )
            for bnr in range(reduced_shape[0]):
                # Warning, auto scale if set by default ping[bnr][:count] = sample_amplitude[start:stop]
                count = len(values[bnr])
                matrix[bnr][:count] = self._apply_scale_offset(values[bnr], scale_factor, add_offset, missing_value)
            return matrix.transpose()

        raise NotImplementedError(
            f"Not supported display of vlen variable {variable_path}, reduced dimensions are too high {reduced_dimensions} ({reduced_shape})"
        )

    @staticmethod
    def get_variable_path_and_name(dataset: nc.Dataset):
        """
        Get a variable name and path, taking into account for Root group being a special dataset
        :return a tuple containing path and name or (None,None) if not a variable
        """
        if isinstance(dataset, nc.Variable):
            parent = dataset._grp
            vpath = getattr(parent, "path", "/")
            return (f"{vpath}/{dataset.name}", dataset.name)
        return None, None

    def walk_tree_group(self, dataset: Optional[nc.Dataset] = None) -> Generator[nc.Group, None, None]:
        if dataset is None:
            dataset = self.dataset
        yield dataset
        for value in dataset.groups.values():
            yield from self.walk_tree_group(value)

    def walk_tree_variables(self, dataset: Optional[nc.Dataset] = None) -> Generator[nc.Variable, None, None]:
        for group in self.walk_tree_group(dataset):
            yield from group.variables.values()

    def walk_tree_vltypes(self, dataset: Optional[nc.Dataset] = None) -> Generator[nc.VLType, None, None]:
        for group in self.walk_tree_group(dataset):
            yield from group.vltypes.values()

    def walk_tree_dimensions(self, dataset: Optional[nc.Dataset] = None) -> Generator[nc.Dimension, None, None]:
        for group in self.walk_tree_group(dataset):
            yield from group.dimensions.values()


@contextmanager
def open_nc_reader(sonar_path: str):
    """
    Define a With Statement Context Managers for a NcReader
    Allow opening a NcReader in a With Statement
    """
    # Code to acquire resource, e.g.:
    sonar_reader = NcReader(sonar_path, True)
    try:
        yield sonar_reader
    finally:
        sonar_reader.close()
