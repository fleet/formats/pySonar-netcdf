import copy
import datetime as dt
import logging
import os
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Union

import netCDF4 as nc
import numpy as np

from .. import sonar_groups as sg

# if run from sonarnetcdf parent directory we need to disable import errors pylint:disable = import-error
# pylint should be run from root directory as pylint sonarnetcdf
from . import nc_encoding
from . import nc_reader as nc_r
from . import nc_variables as nc_v

# Relative import. Useful when access from pyat


class Timeline:
    """
    Simple class to define a timeline, between 2 dates/times
    """

    def __init__(self, name: str, start: dt.datetime, stop: dt.datetime):
        self.name = name
        self.start = start
        self.stop = stop


class MergingSettings:
    """
    variables used by NCMerger during the merging process
    """

    def __init__(self):
        # List of file to merge (filtered, checked and sorted i_paths)
        self.files_to_merge: List[str] = []
        # Descriptions of the dimension in output files. Key is dimension's path
        self.dim_descs: Dict[str, DimensionDescription] = {}
        # Current number of element currently (while merging) stored in variables using the dimension. Key is variable's path
        self.out_var_loading: Dict[str, int] = {}
        # Current cutting period
        self.timeline: Optional[Timeline] = None


class NCMerger:
    """
    Class used for merging generic netcdf files along a time variable.
    May be subclassed to implement specific checks on input files (see _extra_check)
    """

    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        timelines: List[Timeline] = None,
        overwrite: bool = False,
    ) -> None:
        self.i_paths = i_paths
        self.o_paths = o_paths
        self.overwrite = overwrite

        # All cutting periods to apply
        self.timelines = timelines

    def merge(self) -> List[os.PathLike]:
        """
        Main function. Launch the merge process
        Loop over each cutting line and create a merge file.
        Return the list of the resulting files
        """

        files_to_merge = time_sort_files(self.i_paths)

        resulting_files: List[os.PathLike] = []
        error_messages: List[str] = []
        if self.timelines is None or len(self.timelines) == 0:
            # No cutting period. Merge all files together
            error_messages, resulting_files = self._filter_and_merge_files(files_to_merge, self.o_paths[0], None)
        else:
            for o_file, timeline in zip(self.o_paths, self.timelines):
                errors, files = self._filter_and_merge_files(files_to_merge, o_file, timeline)
                for error in errors:
                    error_messages.append(f" - ERROR '{error}'. Output file not produced ({o_file})")
                resulting_files.extend(files)

        if len(error_messages) > 0:
            logging.getLogger(__name__).warning("Summary of error messages :")
            for error in error_messages:
                logging.getLogger(__name__).warning(error)

        return resulting_files

    def _filter_and_merge_files(
        self, files_to_merge: List[Tuple[str, dt.datetime, dt.datetime]], o_file: str, timeline: Optional[Timeline]
    ) -> Tuple[List[str], List[os.PathLike]]:
        """
        Merge all input files and cut the time variables along the specified timeline
        Return a list of errors encountered and the resulting files
        """
        settings = self._check_and_filter_infiles(files_to_merge, timeline)
        return self._merge_on_settings(settings, o_file, timeline)

    def _merge_on_settings(
        self, initial_settings: MergingSettings, o_file: str, timeline: Optional[Timeline]
    ) -> Tuple[List[str], List[os.PathLike]]:
        """
        Process the merge for all parameters contained in settings
        Return a list of errors encountered and the resulting files
        """
        error_messages = []
        resulting_files = []
        settings = copy.deepcopy(initial_settings)
        rejected_files, detail_warn_message = self._compute_dim_desc_size(settings)
        if not settings.files_to_merge:
            logging.getLogger(__name__).warning(f"WARN, all files rejected. Output file not produced ({o_file})")
            return error_messages, resulting_files

        warn_message = ""
        if rejected_files and timeline is not None:
            warn_message = f"WARN : the files belonging to line {timeline.name} have incompatible dimensions. This line will be divided into "

        # Some files have been rejected. We may have several output files
        file_increment = 1 if rejected_files else 0
        while settings.files_to_merge:
            try:
                output_file = self._compute_output_file(o_file, file_increment)
                if file_increment > 0:
                    warn_message = f"{warn_message} {Path(output_file).name}"
                self._process_merge(output_file, settings)
                resulting_files.append(output_file)

                # Try to merge all rejected files each other
                settings = copy.deepcopy(initial_settings)
                settings.files_to_merge = rejected_files
                rejected_files, errors = self._compute_dim_desc_size(settings)
                for msg in errors:
                    detail_warn_message.append(msg)
                file_increment += 1

            except Exception as err:
                error_messages.append(str(err))
                break

        if not error_messages and len(warn_message) > 0:
            logging.getLogger(__name__).warning(warn_message)
            for msg in detail_warn_message:
                logging.getLogger(__name__).warning(f"WARN : {msg}")

        return error_messages, resulting_files

    def _compute_output_file(self, file_path: str, file_increment: int) -> os.PathLike:
        """
        Return a file path composed with the specified file_path and an incremental index
        """
        path = Path(file_path)
        if file_increment > 0:
            suffixes = path.suffixes
            file_name = path.name

            for suffix in suffixes[::-1]:
                file_name = file_name.removesuffix(suffix)
            file_name = f"{file_name}_{file_increment}"
            for suffix in suffixes:
                file_name = file_name + suffix

            path = path.with_name(file_name)

        if path.exists() and not self.overwrite:
            raise SNMergerError(f"File exists and overwite is not allowed : {path.name}")

        return path

    def _process_merge(self, o_file: str, settings: MergingSettings):
        """
        Open each file identified in settings.files_to_merge and merge their content into the output file
        """
        logging.getLogger(__name__).info(f"Merging {len(settings.files_to_merge)} file(s) into {o_file}")
        with nc_encoding.open_nc_file(o_file, mode="w") as out_dataset:
            out_dataset.set_fill_off()
            for sn_path in settings.files_to_merge:
                with nc_r.open_nc_reader(sn_path) as sn_reader:
                    logging.getLogger(__name__).info(f"Processing file : {sn_reader.file_name}")
                    self._append_dataset(out_dataset, sn_reader, settings)

    def _check_and_filter_infiles(
        self, files_to_merge: List[Tuple[str, dt.datetime, dt.datetime]], timeline: Optional[Timeline]
    ) -> MergingSettings:
        """
        Check the content of files in settings.files_to_merge before merging them
        Updates settings.files_to_merge by eliminating wrong input file
        """
        settings = MergingSettings()
        if timeline is not None:
            # Selecting files in timeline
            settings.timeline = timeline
            for infile_path, start_time, stop_time in files_to_merge:
                if stop_time >= timeline.start and start_time <= timeline.stop:
                    settings.files_to_merge.append(infile_path)
        else:
            for infile_path, start_time, stop_time in files_to_merge:
                settings.files_to_merge.append(infile_path)

        ref_extra_cheked_obj = None
        ref_groups: List[str] = []
        ref_dims: List[str] = []
        var_termes: Dict[str, int] = {}

        filtered_files: List[str] = []
        for sn_path in settings.files_to_merge:
            with nc_r.open_nc_reader(sn_path) as sn_reader:
                try:
                    ref_extra_cheked_obj = self._extra_check(sn_reader, ref_extra_cheked_obj)
                    ref_groups = self._check_groups(sn_reader, ref_groups)
                    ref_dims = self._check_dimension_names(sn_reader, ref_dims)
                    self._initialize_dim_descs(sn_reader, settings)
                    self._check_time_variables(sn_reader, var_termes)
                    filtered_files.append(sn_path)
                except SNMergerError as e:
                    logging.getLogger(__name__).warning(f"{sn_path} dismissed. {str(e)}")

        settings.files_to_merge = filtered_files
        return settings

    def _initialize_dim_descs(self, sn_reader: nc_r.NcReader, settings: MergingSettings):
        """Fill settings.dim_descs with all dimensions and coordinate variables of the specified nc file"""
        # First, fill _out_dims with all coordinate variables
        for var in nc_v.find_coordinate_variables(sn_reader):
            for var_dim in var.get_dims():
                dim_path = _get_path(var_dim)
                if dim_path not in settings.dim_descs:
                    dim_desc = DimensionDescription(dim_path)
                    dim_desc.is_coord_var = True
                    if nc_v.has_time_unit(var):
                        dim_desc.is_time_coord_var = True
                    elif var._isprimitive:  # prevent comparing str variable (history)
                        dim_desc.values = var[:]
                    settings.dim_descs[dim_path] = dim_desc

                else:
                    # Check same configuration
                    dim_desc = settings.dim_descs[dim_path]
                    if not dim_desc.is_coord_var or dim_desc.is_time_coord_var != nc_v.has_time_unit(var):
                        raise SNMergerError(f"Difference in dimension interpretation {dim_path}")
                    if dim_desc.values is not None and not np.array_equal(var[:], dim_desc.values):
                        raise SNMergerError(f"Coordinate variable {dim_path} has not the expected values")

        # walk through all dimensions
        for dim in sn_reader.walk_tree_dimensions():
            dim_path = _get_path(dim)
            if dim_path not in settings.dim_descs:
                # Add all other dimensions not already present
                settings.dim_descs[dim_path] = DimensionDescription(dim_path)

            # Check seabed_sample specific case of extensible variable
            if dim.name in [sg.BathymetryGrp.SEABED_SAMPLE_DIM_NAME, sg.BeamGroup1VendorSpecificGrp.SIDESCAN_DIM_NAME]:
                settings.dim_descs[dim_path].is_max_length = True

            # transducer, MRU and position dimension, are static dimensions across mergeable files
            if dim.name in [
                sg.PlatformGrp.MRU_DIM_NAME,
                sg.PlatformGrp.POSITION_DIM_NAME,
                sg.PlatformGrp.TRANSDUCER_DIM_NAME,
            ]:
                settings.dim_descs[dim_path].is_static = True

            # Check for specific state variable such as : SSP, runtime, etc.
            if dim.name in [sg.SoundSpeedProfileGrp.PROFILE_TIME_DIM_NAME]:
                settings.dim_descs[dim_path].is_state_var = True

        # Detect secondary dimensions
        for var in sn_reader.walk_tree_variables():
            if var.ndim > 1:
                for index, var_dim in enumerate(var.get_dims()):
                    if index > 0:
                        dim_path = _get_path(var_dim)
                        settings.dim_descs[dim_path].is_2nd_coord = True

    def _check_time_variables(self, sn_reader: nc_r.NcReader, tmp_var_termes: Dict[str, int]):
        """
        Check that there is no time overlapping
        Raise SNMergerError when time overlap is detected
        tmp_var_termes is used to memorize the last date in variables of previous files
        """
        var_termes = self._get_var_termes(sn_reader)
        if len(tmp_var_termes) > 0:
            for var_path, end_time in var_termes.items():
                if end_time != -1:  # Not empty variable ?
                    previous_period = tmp_var_termes[var_path]
                    if previous_period > end_time:
                        raise SNMergerError(f"Time variables overlap ({var_path})")
                    tmp_var_termes[var_path] = end_time
        else:
            tmp_var_termes.update(var_termes)

    def _compute_dim_desc_size(self, settings: MergingSettings) -> Tuple[List[str], List[str]]:
        """
        Compute dimension of each variable for all file in settings.files_to_merge
        Complete the size attribute of settings.dim_descs
        """
        var_termes: Dict[str, int] = {}
        mergable_files: List[str] = []
        rejected_files: List[str] = []
        errors = []
        for sn_path in settings.files_to_merge:
            if not errors:
                with nc_r.open_nc_reader(sn_path) as sn_reader:
                    try:
                        self._compute_dim_desc_size_for_reader(sn_reader, var_termes, settings)
                        mergable_files.append(sn_path)
                    except SNMergerError as e:
                        # break at the first file with non-compatible dimension encoutered
                        rejected_files.append(sn_path)
                        errors.append(str(e))
            else:
                # then, populate rejected files with the remaining files to merge
                rejected_files.append(sn_path)
        settings.files_to_merge = mergable_files
        # self._dump_dim_sizes(settings)
        return rejected_files, errors

    def _append_dataset(self, out_dataset: nc.Dataset, in_reader: nc_r.NcReader, settings: MergingSettings):
        """
        Duplicates the dataset structure from in file to the out merged one
        """
        self._transfert_group_properties(out_dataset, in_reader.dataset, settings)
        self._merge_variables(out_dataset, in_reader.dataset, settings)
        for in_sub_group in in_reader.dataset.groups.values():
            self._append_sub_group(out_dataset, in_sub_group, settings)

    def _append_sub_group(self, out_parent_group: nc.Group, in_group: nc.Group, settings: MergingSettings):
        """
        Duplicate the group structure from in file to the out merged one
        """
        if in_group.name not in out_parent_group.groups:
            out_group = out_parent_group.createGroup(in_group.name)
            self._transfert_group_properties(out_group, in_group, settings)
        else:
            out_group = out_parent_group.groups[in_group.name]

        self._merge_variables(out_group, in_group, settings)
        for in_sub_group in in_group.groups.values():
            self._append_sub_group(out_group, in_sub_group, settings)

    def _transfert_group_properties(self, out_group: nc.Group, in_group: nc.Group, settings: MergingSettings):
        """
        Duplicates attributes/CompoundType/VLType/EnumType from in group to the out merged one
        """
        # copy attributes
        out_group.set_ncstring_attrs(False)
        out_group.setncatts(in_group.__dict__)

        # copy CompoundType
        for cmp_type in in_group.cmptypes.values():
            if cmp_type.name not in out_group.cmptypes:
                out_group.createCompoundType(cmp_type.dtype, cmp_type.name)
        # copy VLType
        for vlen_type in in_group.vltypes.values():
            if vlen_type.name not in out_group.vltypes:
                out_group.createVLType(vlen_type.dtype, vlen_type.name)
        # copy EnumType
        for enum_type in in_group.enumtypes.values():
            if enum_type.name not in out_group.enumtypes:
                # Trick to ensure the FillValue is defined in the enum
                enum_values = enum_type.enum_dict
                if "Undefined" not in enum_values:
                    enum_values["Undefined"] = -128  # is the value commonly encountered in enums
                out_group.createEnumType(enum_type.dtype, enum_type.name, enum_type.enum_dict)

        self._create_dimensions(out_group, in_group, settings)

    # pylint:disable = unused-argument
    def _extra_check(self, sn_reader: nc_r.NcReader, extra_cheked_obj: Any) -> Any:
        """
        Allow subclass to perform some other checks of input nc file.
        By default, return incoming extra_cheked_obj
        Subclasss may raise SNMergerError when nc file is not suitable to be merged and must be skipped
        """
        return extra_cheked_obj

    def _check_groups(self, sn_reader: nc_r.NcReader, ref_groups: List[str]) -> List[str]:
        """
        Check that the input file has the expected group hierarchy
        Raise SNMergerError when an error is detected
        """
        groups: List[str] = [group.path for group in sn_reader.walk_tree_group()]
        groups.sort()
        if len(ref_groups) > 0:
            if ref_groups != groups:
                raise SNMergerError("Different group hierarchy")
        return groups

    def _check_dimension_names(self, sn_reader: nc_r.NcReader, ref_dims: List[str]):
        """
        Check that input file has the expected dimension names
        Raise SNMergerError when an error is detected
        """
        dims: List[str] = [_get_path(dim) for dim in sn_reader.walk_tree_dimensions()]
        dims.sort()
        if len(ref_dims) > 0:
            if ref_dims != dims:
                raise SNMergerError("Difference in dimension names")
        return dims

    def _slice_time_var(self, time_coord_var, time_values, dim_desc, settings: MergingSettings):
        """
        Determine the cutting slice of the specified variable
        Return a empty slice (slice(0, 0)) if variable is out of timeline

        To take into account the millisecond time precision of the CUT file
        every time series is truncate to milliseconds
        """
        # For debug purpose...
        # if time_coord_var.name == "ping_time" and len(time_coord_var) > 0:
        #     pc.info(f"len(ping_time)={len(time_coord_var)}")
        #    pc.info(f"   from : {nc_v.to_datetime(time_coord_var, time_values[0])}")
        #    middle = int(len(time_coord_var) / 2)
        #    pc.info(f"    via : {nc_v.to_datetime(time_coord_var, time_values[middle])} at index {middle}")
        #    pc.info(f"     to : {nc_v.to_datetime(time_coord_var, time_values[-1])}")

        result = slice(0, len(time_coord_var))
        if settings.timeline:
            # Truncate time variable to milliseconds to take into account the millisecond time precision of the CUT file
            from_date = nc_v.to_millis(time_coord_var, settings.timeline.start)
            to_date = nc_v.to_millis(time_coord_var, settings.timeline.stop)
            before_index = np.argwhere(time_values // np.uint64(1_000_000) < from_date)
            after_index = np.argwhere(time_values // np.uint64(1_000_000) > to_date)

            if len(before_index) > 0:
                if dim_desc.is_state_var:  # retain immediately preceding variable value to keep current state
                    start_index = before_index[-1][0]
                else:  # retain first value within the cut
                    start_index = before_index[-1][0] + 1
                # case of a slice that contains only last value : slice goes from start (inclusive) to stop (exclusive)
                start_index = min(start_index, len(time_coord_var) - 1)
            else:
                start_index = 0

            if len(after_index) > 0:
                if dim_desc.is_state_var:  # retain immediately following variable value to get next state
                    stop_index = after_index[0][0]
                else:  # retain last value within the cut
                    stop_index = after_index[0][0] - 1
            else:
                stop_index = len(time_coord_var)
            # stop index is exclusive, but don't overindex
            stop_index = min(stop_index + 1, len(time_coord_var))

            result = slice(start_index, stop_index)

        return result

    def _compute_dim_desc_size_for_reader(
        self, sn_reader: nc_r.NcReader, var_termes: Dict[str, int], settings: MergingSettings
    ):
        """
        Compute dimension for the output merged file
        Raise SNMergerError when an error is detected
        """
        # Update settings.dim_descs at the end, in case of succes
        dim_descs = copy.deepcopy(settings.dim_descs)
        for dim in sn_reader.walk_tree_dimensions():
            # Sanity check
            if dim.size == 0:
                continue

            dim_path = _get_path(dim)
            dim_desc = dim_descs[dim_path]

            # First time on this dim, initialize dim_desc
            if dim_desc.size == 0:
                if dim_desc.is_time_coord_var:
                    # Store in dim_desc a cutting slice for a time coordinate variable
                    time_coord_var = sn_reader.dataset[dim_path]
                    var_slice = self._slice_time_var(
                        time_coord_var, nc_v.get_time_variable_in_nanos(time_coord_var), dim_desc, settings
                    )
                    dim_desc.slices[os.path.normpath(sn_reader.file_name)] = var_slice
                    time_values = time_coord_var[:]
                    var_termes[dim_path] = time_values[var_slice.stop - 1]
                    dim_desc.size = var_slice.stop - var_slice.start
                else:
                    # Keep the whole variable
                    dim_desc.size = dim.size

            elif dim_desc.is_2nd_coord or (dim_desc.is_coord_var and not dim_desc.is_time_coord_var):
                # Max length dimension. this kind of dimension should take the maximum value across the input files.
                if dim_desc.is_max_length:
                    dim_desc.size = max(dim_desc.size, dim.size)
                # Static dimension. This kind of dimension must have the same value in all input file
                elif dim.size != dim_desc.size:
                    raise SNMergerError(f"Non-compatible Netcdf files, difference size for dimension {dim_path}")

            elif dim_desc.is_static:
                # Static dimension. This kind of dimension must have the same value in all input file
                if dim.size != dim_desc.size:
                    raise SNMergerError(f"Non-compatible Netcdf files, difference size for dimension {dim_path}")

            elif dim_desc.is_time_coord_var:
                # Case of time coordinate variable
                time_coord_var = sn_reader.dataset[dim_path]
                time_values = time_coord_var[:]
                time_values_in_nanos = nc_v.get_time_variable_in_nanos(time_coord_var)
                time_slice = slice(0, dim.size)

                # Exclude overlap
                overlap = np.argwhere(time_values <= var_termes[dim_path])
                if len(overlap) > 0:
                    time_slice = slice(overlap[-1][0] + 1, dim.size)

                # Exclude values outside the range of dates/times
                slice_time_var = self._slice_time_var(time_coord_var, time_values_in_nanos, dim_desc, settings)
                if slice_time_var.stop > 0:
                    time_slice = slice(
                        max(time_slice.start, slice_time_var.start), min(time_slice.stop, slice_time_var.stop)
                    )
                else:
                    # No suitable value in this time variable : slice is empty
                    time_slice = slice_time_var

                dim_desc.slices[os.path.normpath(sn_reader.file_name)] = time_slice
                var_termes[dim_path] = time_values[time_slice.stop - 1]
                dim_desc.size = dim_desc.size + time_slice.stop - time_slice.start
            else:
                # Plain merge case
                dim_desc.size = dim_desc.size + dim.size

        # File is mergable => complete the dimension descriptions
        settings.dim_descs = dim_descs

    def _dump_dim_sizes(self, settings: MergingSettings):
        for dim_desc in settings.dim_descs.values():
            logging.getLogger(__name__).info(str(dim_desc))

    def _get_var_termes(self, sn_reader: nc_r.NcReader) -> Dict[str, int]:
        """
        Return a description of time variable for the specified Netcdf file
        Result is a dict of ending_time, Key is variable path
        """
        result: Dict[str, int] = {}
        for time_var in nc_v.find_time_coordinate_variables(sn_reader):
            if len(time_var) > 0:
                times = time_var[:]
                result[_get_path(time_var)] = times[-1]
            else:
                result[_get_path(time_var)] = -1

        return result

    def _merge_one_variable(self, out_var: nc.Variable, in_var: nc.Variable, settings: MergingSettings):
        var_path = _get_path(out_var)
        loading = settings.out_var_loading[var_path] if var_path in settings.out_var_loading else 0

        if in_var.ndim == 0:  # Case of variable with no dimension
            if loading == 0:  # only value in first file is kept
                out_var[:] = in_var[:]
                settings.out_var_loading[var_path] = 1
            elif out_var[:] != in_var[:]:
                logging.getLogger(__name__).warning(
                    f"WARN, scalar variable {var_path} changed. only value in first file is kept"
                )
            return

        if loading >= len(out_var):
            # Case of static variable : only values in first file are kept

            # String variable was concatenated
            # --> No longer useful since xsf history is stored as an attribute
            # if out_var.dtype == str:
            #     str_val = out_var[:] + "\n" + in_var[:]
            #     out_var[:] = str_val
            return

        in_var.set_auto_maskandscale(False)

        try:
            values = in_var[:]
            if out_var._isenum:
                # Ensure all values belong to the enum
                enum = out_var.datatype.enum_dict
                enum_values = list(enum.values())
                bad_values_mask = np.isin(values, enum_values, invert=True)
                values[bad_values_mask] = enum["Undefined"]

            # Determine the cutting slice of the input variable.
            # By default, all values
            val_slice = slice(0, in_var.shape[0])

            # Determine the cutting slice of the output variable.
            # By default, all input values without start offset
            out_slice = [slice(0, dim_size) for dim_size in in_var.shape]

            # Slice may exist for a time coordinate variable (in case of time overlapping)
            in_dim_path = _get_path(in_var.get_dims()[0])
            dim_desc = settings.dim_descs[in_dim_path]

            if dim_desc.is_time_coord_var:
                filepath = os.path.normpath(nc_encoding.filepath(nc_v.find_datatset_of_var(in_var)))
                if filepath in dim_desc.slices:
                    val_slice = dim_desc.slices[filepath]

            # Transfer data
            # pc.info(f"Adding {val_slice.stop - val_slice.start} values to variable : {var_path}")
            slice_size = val_slice.stop - val_slice.start
            out_slice[0] = slice(loading, loading + slice_size)

            if dim_desc.size >= loading + slice_size:
                out_var.set_auto_maskandscale(False)
                out_var[out_slice] = values[val_slice]
                settings.out_var_loading[var_path] = loading + slice_size
            else:
                logging.getLogger(__name__).error(
                    f"ERROR, abnormal case. Try to add more values in {var_path} than expected"
                )
        except UnicodeDecodeError:
            logging.getLogger(__name__).warning(f"WARN, variable {var_path} is not in UTF-8 format. Skipped.")

    def _merge_variables(self, out_group: nc.Group, in_group: nc.Group, settings: MergingSettings):
        for in_var in in_group.variables.values():
            if in_var.name not in out_group.variables:
                self._create_variables(out_group, in_var, settings)
            out_var = out_group.variables[in_var.name]
            if in_var.size > 0:
                self._merge_one_variable(out_var, in_var, settings)

    def _find_enum_type(self, group: nc.Group, enum_name: str) -> nc.EnumType:
        if enum_name in group.enumtypes:
            return group.enumtypes[enum_name]
        return self._find_enum_type(group.parent, enum_name)

    def _find_vlen_type(self, group: nc.Group, vlen_name: str) -> nc.VLType:
        if vlen_name in group.vltypes:
            return group.vltypes[vlen_name]
        return self._find_vlen_type(group.parent, vlen_name)

    def _create_variables(self, out_group: nc.Group, in_var: nc.Variable, settings: MergingSettings):
        var_attrs = in_var.__dict__
        # retrieve compression parameters
        zlib = None
        complevel = sg.DEFAULT_COMPRESSION_LEVEL
        if in_var.filters() is not None and not in_var._isvlen:
            if in_var.filters().get(sg.DEFAULT_COMPRESSION_LIB, False):
                zlib = sg.DEFAULT_COMPRESSION_LIB
            complevel = in_var.filters().get("complevel", sg.DEFAULT_COMPRESSION_LEVEL)
        # copy only first dimension chunk to handle dimensions change during merge process
        chunksizes = None
        if in_var.chunking() is not None:
            if in_var.chunking() != "contiguous" and tuple(in_var.chunking())[0] != in_var.shape[0]:
                chunksizes = np.full(len(in_var.shape), fill_value=0)
                # retrieve new dim size in case of cut (chunksize can't be greather than dim size)
                in_dim_path = _get_path(in_var.get_dims()[0])
                dim_desc = settings.dim_descs[in_dim_path]
                chunksizes[0] = min(tuple(in_var.chunking())[0], dim_desc.size)

        if in_var._isvlen and in_var.dtype != str:
            vlen_t = self._find_vlen_type(out_group, in_var.datatype.name)
            out_var = out_group.createVariable(in_var.name, vlen_t, in_var.dimensions)
            out_var.setncatts(var_attrs)
        elif in_var._isenum:
            # Specific case of enum : replace FillValue by "Undefined"
            enum_t = self._find_enum_type(out_group, in_var.datatype.name)
            out_var = out_group.createVariable(
                in_var.name,
                enum_t,
                in_var.dimensions,
                fill_value=enum_t.enum_dict["Undefined"],
                compression=zlib,
                complevel=complevel,
                chunksizes=chunksizes,
            )
            out_var.setncatts({att: att_val for (att, att_val) in var_attrs.items() if att != "_FillValue"})
        elif "_FillValue" in var_attrs:
            fillvalue = var_attrs["_FillValue"]
            # Specify the fill value at variable creation
            out_var = out_group.createVariable(
                in_var.name,
                in_var.datatype,
                in_var.dimensions,
                fill_value=fillvalue,
                compression=zlib,
                complevel=complevel,
                chunksizes=chunksizes,
            )
            out_var.setncatts({att: att_val for (att, att_val) in var_attrs.items() if att != "_FillValue"})
            out_var.set_auto_maskandscale(False)
            # init variable with fill value
            out_var[:] = np.full(shape=out_var.shape, fill_value=fillvalue)
        else:
            out_var = out_group.createVariable(
                in_var.name,
                in_var.datatype,
                in_var.dimensions,
                compression=zlib,
                complevel=complevel,
                chunksizes=chunksizes,
            )
            out_var.setncatts(var_attrs)

    def _create_dimensions(self, out_group: nc.Group, in_group: nc.Group, settings: MergingSettings):
        for in_dim in in_group.dimensions.values():
            if in_dim.name not in out_group.dimensions:
                in_dim_path = _get_path(in_dim)
                out_group.createDimension(in_dim.name, settings.dim_descs[in_dim_path].size)


class DimensionDescription:
    """
    Description of a dimension as it will be in the merged file
    """

    def __init__(self, path: str):
        # Path of dim in file
        self.path = path
        # Computed size of this dimension
        self.size = 0
        # True when dimension corresponds to a coordinates variable
        self.is_coord_var = False
        # True when the dimension is used in conjunction with an other main dimension
        self.is_2nd_coord = False
        # True when dimension corresponds to a coordinates variable with a time unit
        self.is_time_coord_var = False
        # True when the dimension represents the max length of a variable (extensible)
        self.is_max_length = False
        # True when the dimension is static and should remain the same in each file
        self.is_static = False
        # True when the dimension corresponds to a state variable (e.g. soundspeed profile, runtime, etc
        self.is_state_var = False
        # Values of a coordinate variable. When present, must be the same for all files
        self.values: Optional[np.ndarray] = None
        # Dictionnary of slices for cutting variables using the dimension as main coordinates variable (manage time overlapping)
        self.slices: Dict[str, Any] = {}

    def __repr__(self):
        if self.is_2nd_coord:
            return f"{self.path} : detected as second axis of a variable. Size is {self.size}"
        elif self.is_time_coord_var:
            return f"{self.path} : coordinate variable of time. Merging size is {self.size}"
        elif self.is_coord_var:
            return f"{self.path} : coordinate variable. Size is {self.size}"
        return f"Dimension detected {self.path}. Merging size is {self.size}"


class SNMergerError(Exception):
    """
    Kind of exception raised when an uncompatibility between files to merge was detected
    """


def time_sort_files(file_list: List[str]) -> List[Tuple[str, dt.datetime, dt.datetime]]:
    """
    Sort all files in file_list (i.e self.i_paths) according to one of the time coordinate variables (favorite is ping_time)
    Wrong files (unable to open with Netcdf) are excluded
    """
    # Create a list of tuple (File_name, starting date, ending date)
    files_to_sort: List[Tuple[str, dt.datetime, dt.datetime]] = []
    sorting_variable = None
    for sn_path in file_list:
        try:
            with nc_r.open_nc_reader(sn_path) as sn_reader:
                if sorting_variable is None:
                    # First file. Determine a time variable used to sort the files
                    time_coord_vars = list(nc_v.find_time_coordinate_variables(sn_reader))
                    if len(time_coord_vars) == 0:
                        raise SNMergerError("No time coordinate variable found")

                    # Favorite sort time variable is ping_time
                    sorting_variable = _get_path(
                        next((var for var in time_coord_vars if var.name == "ping_time"), time_coord_vars[0])
                    )
                    logging.getLogger(__name__).info(f"Sorting file on variable {sorting_variable}")

                time_coord_var = sn_reader.find_variable(sorting_variable)
                if time_coord_var is not None:
                    if len(time_coord_var) > 0:
                        time_values_in_nanos = nc_v.get_time_variable_in_nanos(time_coord_var)
                        files_to_sort.append(
                            (
                                sn_path,
                                nc_v.to_datetime(time_coord_var, time_values_in_nanos[0]),
                                nc_v.to_datetime(time_coord_var, time_values_in_nanos[-1]),
                            )
                        )
                    else:
                        logging.getLogger(__name__).warning(
                            f"{sn_path} discarded : variable {sorting_variable} is empty"
                        )
                else:
                    logging.getLogger(__name__).warning(f"{sn_path} discarded : variable {sorting_variable} not found")
        except OSError as e:
            logging.getLogger(__name__).warning(f"{sn_path} discarded : {str(e)}")

    files_to_sort.sort(key=lambda tup: tup[1])
    return files_to_sort


def _get_group_path(group: nc.Group) -> str:
    return group.path if group.parent else ""


def _get_path(var: Union[nc.Variable, nc.Dimension]) -> str:
    return _get_group_path(var.group()) + "/" + var.name


# in case we are started in standalone app, for debug only
# launch with "python -m sonar_netcdf.utils.nc_merger"
if __name__ == "__main__":
    folder = "E:/ifremer/data/techsas/ADCP"
    sn_paths = [str(Path(folder, sn_path)) for sn_path in os.listdir(folder)]
    merger = NCMerger(
        sn_paths,
        ["E:/temp/line_1.nc", "E:/temp/line_1.nc"],
        timelines=[
            Timeline(
                "line1",
                dt.datetime(2017, 5, 21, 5, 21, 51, tzinfo=dt.timezone.utc),
                dt.datetime(2017, 5, 22, 23, 47, 42, tzinfo=dt.timezone.utc),
            ),
            Timeline(
                "line2",
                dt.datetime(2017, 5, 24, 2, 2, 14, tzinfo=dt.timezone.utc),
                dt.datetime(2017, 6, 9, 18, 13, 48, tzinfo=dt.timezone.utc),
            ),
        ],
    )
    merger.merge()
