class bcolors:
    """
    Utility for color display in terminal or jupyter
    print(f"{bcolors.WARNING}{text}{bcolors.ENDC}")
    """

    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[1;95m"
    FAIL = "\033[1;91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


def info(msg):
    pprint(msg)


def pprint(msg, back_ground_color=None):
    """Intercept print call"""
    if back_ground_color is None:
        print(msg)
    else:
        print(f"{back_ground_color}{msg}{bcolors.ENDC}")


def error(msg: str):
    pprint(msg, back_ground_color=bcolors.FAIL)


def warning(msg: str):
    pprint(msg, back_ground_color=bcolors.WARNING)


def header(msg: str):
    pprint(msg, back_ground_color=bcolors.BOLD)
