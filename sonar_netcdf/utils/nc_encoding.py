import locale
from os import PathLike

import netCDF4 as nc


def open_nc_file(file_path: PathLike, mode="r", nc_format="NETCDF4") -> nc.Dataset:
    """
    Opens a netCDF file after checking the path encoding
    """
    return nc.Dataset(file_path, mode=mode, format=nc_format, encoding=locale.getpreferredencoding())


def filepath(dataset: nc.Dataset) -> nc.Dataset:
    """
    Return the file system path which was used to open/create the Dataset.
    """
    return dataset.filepath(encoding=locale.getpreferredencoding())
