"""
Utility class to manage Sonar-netcdf variables
"""
import datetime as dt
from typing import Generator

import netCDF4 as nc
import numpy as np

# pylint issue with relative import https://github.com/PyCQA/pylint/issues/3651
# pylint: disable=import-error
from . import nc_reader as nc_r


# pylint: enable=import-error


def find_coordinate_variables(reader: nc_r.NcReader) -> Generator[nc.Variable, None, None]:
    """
    Browse downward group hierarchy to find all coordinate variables
    """
    for variable in reader.walk_tree_variables():
        # Coordinate variables have only one dimension
        # A variable with the same name as a dimension is called a coordinate variable
        if variable.ndim == 1 and variable.dimensions[0] == variable.name:
            yield variable


def has_time_unit(variable: nc.Variable) -> bool:
    """
    Return True when variable contains date/time
    """
    return "units" in variable.__dict__ and variable.units in [
        "nanoseconds since 1601-01-01 00:00:00Z",
        "nanoseconds since 1970-01-01 00:00:00Z",
        "days since 1899-12-30 00:00:00 UTC",
        "days since 1899-12-30T00:00:00+00:00"
    ]


def find_time_coordinate_variables(reader: nc_r.NcReader) -> Generator[nc.Variable, None, None]:
    """
    Browse downward group hierarchy to find all coordinate variables based on time units
    """
    for variable in find_coordinate_variables(reader):
        # time coordinate variables have a specific unit
        if has_time_unit(variable):
            yield variable


def find_datatset_of_group(group: nc.Group) -> nc.Dataset:
    """
    Browse upward group hierarchy to reach root dataset
    """
    if group.parent is not None:
        return find_datatset_of_group(group.parent)
    return group


def find_datatset_of_var(variable: nc.Variable) -> nc.Dataset:
    """
    Browse upward group hierarchy to reach root dataset
    """
    return find_datatset_of_group(variable.group())


def get_origin(variable: nc.Variable) -> dt.datetime:
    """
    Return the datetime corresponding to origin of the variable
    """
    # nanoseconds since 1601-01-01 00:00:00Z
    if "1601" in variable.units:
        return dt.datetime(1601, 1, 1, 0, 0, 0, tzinfo=dt.timezone.utc)
        # days since 1899-12-30 00:00:00 UTC
    if "1899" in variable.units:
        return dt.datetime(1899, 12, 30, 0, 0, 0, tzinfo=dt.timezone.utc)
    # nanoseconds since 1970-01-01 00:00:00Z
    return dt.datetime(1970, 1, 1, 0, 0, 0, tzinfo=dt.timezone.utc)


def to_datetime(variable: nc.Variable, nanos: int) -> dt.datetime:
    """
    Return the datetime corresponding to the timestamp according to the variable units
    """
    return get_origin(variable) + dt.timedelta(microseconds=nanos / 1000)


def to_nanos(variable: nc.Variable, date: dt.datetime) -> int:
    """
    Return the timestamp in nanoseconds corresponding to the datetime according to the variable units
    """
    return int((date - get_origin(variable)) / dt.timedelta(microseconds=1)) * 1000


def to_millis(variable: nc.Variable, date: dt.datetime) -> int:
    """
    Return the timestamp in milliseconds corresponding to the datetime according to the variable units
    """
    return int((date - get_origin(variable)) / dt.timedelta(milliseconds=1))


def get_time_variable_in_nanos(variable: nc.Variable) -> np.ndarray:
    """
    Return the time variable in nanoseconds
    """
    result = variable[:]
    if "days" in variable.units:
        result = np.array(result * dt.timedelta(days=1).total_seconds() * 1_000_000_000, dtype=np.uint64)
    return result


if __name__ == "__main__":
    with nc_r.open_nc_reader("E:/ifremer/data/sonar-netcdf/HYDROMOMAR/HYDROMOMAR-D20200904-T093757.nc") as nc_reader:
        for var in find_time_coordinate_variables(nc_reader):
            print(var.name)
