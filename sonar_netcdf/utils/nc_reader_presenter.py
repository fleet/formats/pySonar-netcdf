import sys
import traceback

import matplotlib.pyplot as plt
import netCDF4 as nc
import numpy as np


#pylint issue with relative import https://github.com/PyCQA/pylint/issues/3651
# pylint: disable=import-error
from . import nc_reader as nc_r
from .print_color import error, header, pprint, warning
# pylint: enable=import-error


# ensure minimum size for figures
plt.rcParams["figure.dpi"] = 270

# pylint:disable=redefined-outer-name


class NcReaderPresenter:
    """
    Class used for reading a Sonar-netcdf like file
    It has a set of methods allowing to parse and dump the file content on a jupyter netbook
    """

    def __init__(self, reader: nc_r.NcReader):
        """
        :param reader: instance of NcReader to present
        """
        self.reader = reader

    def _display_variable_header(self, variable_name, variable_path):
        """
        Print variable
        :return: None
        """
        if not self.reader.quiet:
            header("\n\n")
            header(f"Variable {variable_name} : {variable_path}")
            pprint(f"{self.reader.dataset[variable_path]}")

    # pylint: disable=too-many-nested-blocks
    def _display_variable(
        self,
        variable_name,
        variable_path,
        slice_index: dict,
        ignore_variable=None,
        cmap="viridis",
        vmin=None,
        vmax=None,
    ) -> np.ndarray:
        if ignore_variable is None:
            ignore_variable = []
        if cmap is None:
            cmap = "viridis"
        # this is the only way I found to clear the figure : clear it and recreate it
        if not self._is_ignored(variable_name, ignore_variable):
            if self._is_string_variable(variable_path):
                # String variable, will only pprint a few data
                pprint(f"Variable {variable_name} is of type string")
                variable = self.reader.dataset[variable_path]
                values = variable[:]
                pprint(f"Variable {variable_name} {len(values)} values (['{values[0]}',...,'{values[-1]}']")
            else:
                v = self.reader._get_variable_data(variable_path, slice_index)
                if not self.reader.quiet:
                    pprint("Variable : " + variable_path + " size =" + str(v.shape))

                if len(v.shape) == 1:
                    if v.shape[0] > 0:
                        if not self.reader.quiet:
                            pprint("Statistics min:" + str(np.nanmin(v)) + " max:" + str(np.nanmax(v)))
                            plt.plot(v)
                            plt.show()
                    else:
                        warning("1D variable with a null size" + variable_path + " size =" + str(v.shape))
                elif len(v.shape) == 2:
                    if v.shape[0] > 0 and v.shape[1] > 0:
                        if not self.reader.quiet:
                            pprint("statistics min:" + str(np.nanmin(v)) + " max:" + str(np.nanmax(v)))
                            if vmin is None:
                                vmin = np.nanmin(v)
                            if vmax is None:
                                vmax = np.nanmax(v)
                            fig, ax = plt.subplots()
                            im = ax.imshow(v, aspect="auto", cmap=cmap, vmin=vmin, vmax=vmax)
                            fig.colorbar(im)
                            plt.show()
                    else:
                        warning(f"Empty values dimensions = {v.shape}")
                else:
                    error("Cannot display variable : " + variable_path + " size =" + str(v.shape))
                    pprint("Values =" + str(v))

                return v
        else:
            warning("ignored variable :" + variable_name)
        return None

    def dump_content(
        self,
        root="/",
        recurse_subgroup=True,
        slice_index=None,
        ignored_variable_list=None,
        cmap=None,
        vmax=None,
        vmin=None,
    ):
        """
        Display all group content (name, types, attributes, variables)
        Variable content is displayed if numeric as 1D or 2D plots.
        When dimensions of variable is higher that 2 a reduction is done on dimension equals to 1 and if not enough the slice_index parameter is used to reduce dimensions
        Vlen data is filled up with invalid values along the variable length dimension in order to be displayed properly
        :param root: the root path used as a starting point
        :param recurse_subgroup: boolean indicating if recursion into subgroup is done
        :param slice_index: parameter containing a dictionnary of index that should be use to reduce dimension if needed for example slice_index={'ping_time':3}
        :param ignored_variable_list: list of variables that should be ignored
        :return: None
        """
        if slice_index is None:
            slice_index = {}
        if ignored_variable_list is None:
            ignored_variable_list = []
        if root == "/" or root is None:
            root_group = self.reader.dataset
        else:
            root_group = self.reader.dataset[root]

        return self._recurse_and_display(
            root_group,
            slice_index=slice_index,
            recurse_subgroup=recurse_subgroup,
            ignored_variable_list=ignored_variable_list,
            cmap=cmap,
            vmin=vmin,
            vmax=vmax,
        )

    def dump_groups(self, starting_path="/"):
        """
        Parse recursively all group and print their names
        :param starting_path: the group starting point
        :return: None
        """
        if starting_path == "/" or starting_path is None:
            self._dump_groups_and_recurse(group=None)
            # Root group is not strictly a group but a nc.Dataset (it has no name)
            start_dataset = self.reader.dataset
        else:
            start_dataset = self.reader.dataset[starting_path]
            self._dump_groups_and_recurse(start_dataset)

    def _dump_groups_and_recurse(self, group=None, level=""):
        """Recurse every group and print their names"""
        if group is None:
            pprint("-+Root")
            dataset = self.reader.dataset
        else:
            if len(group.groups) > 0:
                pprint(f"{level}|+{group.name}")
            else:
                pprint(f"{level}|-{group.name}")
            dataset = group

        for subgroup_name in dataset.groups:
            self._dump_groups_and_recurse(dataset[subgroup_name], level=f"{level} ")

    def _print_variable(self, variable, slice_index, ignored_variable_list, cmap=None, vmin=None, vmax=None):
        v = None
        try:
            variable_path = variable._grp.path + "/" + variable.name
            self._display_variable_header(variable, variable_path)
            v = self._display_variable(
                variable,
                variable_path,
                slice_index,
                ignore_variable=ignored_variable_list,
                cmap=cmap,
                vmin=vmin,
                vmax=vmax,
            )
        except:  # pylint: disable=W0702
            error("Error for variable :" + variable_path)
            error(f"Unexpected error: {sys.exc_info()}")
            traceback.print_exc()
        return v

    def _recurse_and_display(
        self, dataset, slice_index, recurse_subgroup=True, ignored_variable_list=None, cmap=None, vmin=None, vmax=None
    ):
        last_variable = None
        if ignored_variable_list is None:
            ignored_variable_list = []
        if isinstance(dataset, nc.Variable):
            return self._print_variable(dataset, slice_index, ignored_variable_list, cmap=cmap, vmin=vmin, vmax=vmax)
        if dataset.parent is not None:
            header(f"Group {dataset.name} ({dataset.path}")
        else:
            # root dataset has not name
            header(f"Root Group {dataset.path}")
        if not self.reader.quiet:
            pprint(f"{dataset}")
        for variable in sorted(dataset.variables):
            last_variable = self._print_variable(
                dataset[variable], slice_index, ignored_variable_list, cmap=cmap, vmin=vmin, vmax=vmax
            )

        if recurse_subgroup:
            for subgroup_name in dataset.groups:
                last_variable = self._recurse_and_display(
                    dataset.groups[subgroup_name], slice_index, cmap=cmap, vmin=vmin, vmax=vmax
                )
        return last_variable

    def _is_ignored(self, variable_name, ignore_variable):
        """
        Tell is a variable is masked variable (ancillary variable for example)
        """
        return variable_name in ignore_variable

    def _is_string_variable(self, variable_path):
        variable = self.reader.dataset[variable_path]
        return variable.dtype == type("str")


# in case we are started in standalone app, for debug only
if __name__ == "__main__":
    # due to relative import should be run as a module (python -m ...)

    file_path = "D:/data/file/XSF/Movies/Sardine_schools_1.xsf.nc"
    file_path = "D:/XSF/0006_20200504_111056_FG_EM122.xsf.nc"
    file_path = "D:/data/file/XSF/ExampleSonarData/test90-D20171107-T195133.nc"
    file_path = "C:/data/datasets/ADCP/HYDROMOMAR-D20200904-T093759.nc"
    file_path = "C:/data/datasets/GriddedData/ACOU03d161608_1.xsf.nc"

    start = "/"
    reader = nc_r.NcReader(file_path)
    presenter = NcReaderPresenter(reader)
    values = presenter.dump_content(
        root="Sonar/Grid_group_1/integrated_backscatter", slice_index={"frequency": 1}, cmap="jet", vmin=-80, vmax=-30
    )
