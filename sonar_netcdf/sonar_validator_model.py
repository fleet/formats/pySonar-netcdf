"""
Utility class generated see for Sonar-netcdf see https://gitlab.ifremer.fr/fleet/formats/pySonar-netcdf
"""


import netCDF4 as nc

import numpy as np


# pylint: disable=E1101
# pylint: disable=R0904
# pylint: disable=C0305
# pylint: disable=C0302

#Obligation fields definition
mandatory = "mandatory"
recommended = "recommended"
optional = "optional"
mandatory_if_applicable = "mandatory_if_applicable"

class RootGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return "/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={"Conventions": mandatory,"date_created": mandatory,"keywords": mandatory,"license": optional,"rights": optional,"sonar_convention_authority": mandatory,"sonar_convention_name": mandatory,"sonar_convention_version": mandatory,"summary": mandatory,"title": mandatory,"processing_status": optional,"xsf_convention_version": optional}

    #Dimensions 
    _dimensions={}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables= {}

    #Sub groups
class AnnotationGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return RootGrpValidator.get_group_path_pattern()+"Annotation/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={"time": mandatory_if_applicable}

    #Variables
    _variables={"annotation_category": optional,"annotation_text": mandatory_if_applicable}
class EnvironmentGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return RootGrpValidator.get_group_path_pattern()+"Environment/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"frequency": mandatory}

    #Variables
    _coordinate_variables={"frequency": mandatory}

    #Variables
    _variables={"absorption_indicative": mandatory,"sound_speed_indicative": mandatory}

    #Sub groups
class SoundSpeedProfileGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return EnvironmentGrpValidator.get_group_path_pattern()+"Sound_speed_profile/"
    _enum_types=[]
    _vlen_types=["float_sample"]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"profile_time": mandatory}

    #Variables
    _coordinate_variables={"profile_time": mandatory}

    #Variables
    _variables={"measure_time": optional,"lat": optional,"lon": optional,"sample_depth": mandatory,"sample_count": mandatory,"sound_speed": mandatory,"temperature": optional,"salinity": mandatory,"absorption": optional}

    #Sub groups
class SurfaceSoundSpeedGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return EnvironmentGrpValidator.get_group_path_pattern()+"Surface_sound_speed/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"surface_sound_speed": mandatory,"time": mandatory}

    #Sub groups
class TideGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return EnvironmentGrpValidator.get_group_path_pattern()+"Tide/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"tide_indicative": mandatory,"time": mandatory}

    #Sub groups
class PlatformGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return RootGrpValidator.get_group_path_pattern()+"Platform/"
    _enum_types=["transducer_type_t"]
    _vlen_types=[]

    #Attributes
    _attributes={"platform_code_ICES": optional,"platform_name": optional,"platform_type": optional}

    #Dimensions 
    _dimensions={"transducer": mandatory,"position": mandatory,"MRU": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"MRU_offset_x": recommended,"MRU_offset_y": recommended,"MRU_offset_z": recommended,"MRU_rotation_x": recommended,"MRU_rotation_y": recommended,"MRU_rotation_z": recommended,"MRU_ids": mandatory_if_applicable,"position_ids": mandatory_if_applicable,"position_offset_x": recommended,"position_offset_y": recommended,"position_offset_z": recommended,"transducer_offset_x": recommended,"transducer_offset_y": recommended,"transducer_offset_z": recommended,"transducer_ids": mandatory_if_applicable,"transducer_rotation_x": recommended,"transducer_rotation_y": recommended,"transducer_rotation_z": recommended,"transducer_function": mandatory,"water_level": recommended}

    #Sub groups
class PositionGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return PlatformGrpValidator.get_group_path_pattern()+"Position/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables= {}

    #Sub groups
class PositionSubGroupValidator:
    @staticmethod
    def get_group_path_pattern():
        return PositionGrpValidator.get_group_path_pattern()+".*"+"/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={"description": optional}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={"time": mandatory}

    #Variables
    _variables={"altitude": mandatory_if_applicable,"course_over_ground": optional,"distance": optional,"heading": mandatory_if_applicable,"height_above_reference_ellipsoid": mandatory_if_applicable,"latitude": mandatory,"longitude": mandatory,"speed_over_ground": mandatory_if_applicable,"speed_relative": mandatory_if_applicable}

    #Sub groups
class NmeaGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return PositionSubGroupValidator.get_group_path_pattern()+"NMEA/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={"description": mandatory}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={"time": mandatory}

    #Variables
    _variables={"NMEA_datagram": optional}
class AttitudeGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return PlatformGrpValidator.get_group_path_pattern()+"Attitude/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables= {}

    #Sub groups
class AttitudeSubGroupValidator:
    @staticmethod
    def get_group_path_pattern():
        return AttitudeGrpValidator.get_group_path_pattern()+".*"+"/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={"description": optional}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={"time": mandatory}

    #Variables
    _variables={"heading": mandatory_if_applicable,"heading_rate": mandatory_if_applicable,"pitch": mandatory,"pitch_rate": optional,"roll": mandatory,"roll_rate": optional,"vertical_offset": mandatory}

    #Sub groups
class DynamicDraughtGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return PlatformGrpValidator.get_group_path_pattern()+"Dynamic_draught/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"time": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"delta_draught": mandatory,"time": mandatory}
class ProvenanceGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return RootGrpValidator.get_group_path_pattern()+"Provenance/"
    _enum_types=[]
    _vlen_types=[]

    #Attributes
    _attributes={"conversion_software_name": mandatory_if_applicable,"conversion_software_version": mandatory_if_applicable,"conversion_time": mandatory_if_applicable,"history": recommended}

    #Dimensions 
    _dimensions={"filenames": mandatory_if_applicable}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"source_filenames": mandatory_if_applicable}
class SonarGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return RootGrpValidator.get_group_path_pattern()+"Sonar/"
    _enum_types=["beam_stabilisation_t","beam_t","conversion_equation_t","transmit_t"]
    _vlen_types=[]

    #Attributes
    _attributes={"sonar_manufacturer": recommended,"sonar_model": recommended,"sonar_serial_number": recommended,"sonar_software_name": recommended,"sonar_software_version": recommended,"sonar_type": mandatory}

    #Dimensions 
    _dimensions={}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables= {}

    #Sub groups
class BeamGroup1GrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return SonarGrpValidator.get_group_path_pattern()+"Beam_group[0-9]*"+"/"
    _enum_types=[]
    _vlen_types=["sample_t","angle_t","pulse_t"]

    #Attributes
    _attributes={"beam_mode": mandatory,"conversion_equation_type": mandatory,"preferred_MRU": mandatory_if_applicable,"preferred_position": mandatory_if_applicable}

    #Dimensions 
    _dimensions={"beam": mandatory,"subbeam": mandatory,"ping_time": mandatory,"tx_beam": mandatory,"frequency": mandatory}

    #Variables
    _coordinate_variables={"beam": mandatory,"ping_time": mandatory,"frequency": mandatory_if_applicable}

    #Variables
    _variables={"active_MRU": mandatory_if_applicable,"active_position_sensor": mandatory_if_applicable,"backscatter_i": mandatory_if_applicable,"backscatter_r": mandatory,"beam_stabilisation": mandatory,"beam_type": mandatory,"beamwidth_receive_major": mandatory,"beamwidth_receive_minor": mandatory,"beamwidth_transmit_major": mandatory_if_applicable,"beamwidth_transmit_minor": mandatory_if_applicable,"blanking_interval": mandatory,"calibrated_frequency": mandatory,"detected_bottom_range": optional,"echoangle_major": mandatory_if_applicable,"echoangle_major_sensitivity": mandatory_if_applicable,"echoangle_minor": mandatory_if_applicable,"echoangle_minor_sensitivity": mandatory_if_applicable,"equivalent_beam_angle": mandatory,"gain_correction": mandatory_if_applicable,"non_quantitative_processing": mandatory,"platform_heading": mandatory,"platform_latitude": mandatory,"platform_longitude": mandatory,"platform_pitch": mandatory,"platform_roll": mandatory,"platform_vertical_offset": mandatory,"receive_duration_effective": mandatory_if_applicable,"receive_transducer_index": mandatory_if_applicable,"receiver_sensitivity": mandatory_if_applicable,"rx_beam_rotation_phi": mandatory,"rx_beam_rotation_psi": mandatory,"rx_beam_rotation_theta": mandatory,"sample_count": optional,"sample_interval": mandatory,"sample_time_offset": mandatory,"sound_speed_at_transducer": optional,"time_varied_gain": mandatory_if_applicable,"transceiver_impedance": mandatory_if_applicable,"transducer_gain": mandatory_if_applicable,"transducer_impedance": mandatory_if_applicable,"transmit_bandwidth": optional,"transmit_beam_index": mandatory_if_applicable,"transmit_duration_nominal": mandatory,"transmit_pulse_model_i": mandatory_if_applicable,"transmit_frequency_start": mandatory,"transmit_frequency_stop": mandatory,"transmit_power": mandatory_if_applicable,"transmit_pulse_model_r": mandatory_if_applicable,"transmit_source_level": mandatory_if_applicable,"transmit_transducer_index": mandatory_if_applicable,"transmit_type": mandatory,"transmitter_and_receiver_coefficient": mandatory_if_applicable,"tx_beam_rotation_phi": mandatory,"tx_beam_rotation_psi": mandatory,"tx_beam_rotation_theta": mandatory,"tx_transducer_depth": optional,"waterline_to_chart_datum": optional}

    #Sub groups
class AdcpGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return BeamGroup1GrpValidator.get_group_path_pattern()+"ADCP/"
    _enum_types=[]
    _vlen_types=["sample_v"]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"backscatter_at_bottom_i": mandatory_if_applicable,"backscatter_at_bottom_r": mandatory_if_applicable,"bin_length": mandatory_if_applicable,"bottom_track_velocity_vessel_x": mandatory_if_applicable,"bottom_track_velocity_vessel_y": mandatory_if_applicable,"bottom_track_velocity_vessel_z": mandatory_if_applicable,"correlation": mandatory_if_applicable,"correlation_at_bottom": mandatory_if_applicable,"correlation_factor_limit": mandatory_if_applicable,"current_velocity_geographical_down": mandatory_if_applicable,"current_velocity_geographical_east": mandatory_if_applicable,"current_velocity_geographical_north": mandatory_if_applicable,"current_velocity_vessel_x": optional,"current_velocity_vessel_y": optional,"current_velocity_vessel_z": optional,"depth_first_sample_center": mandatory_if_applicable,"error_velocity": optional,"error_velocity_limit": mandatory_if_applicable,"quality": mandatory_if_applicable,"scaling_factor": mandatory_if_applicable,"slant_range_to_bottom": mandatory_if_applicable,"sv_dbw_high_limit": mandatory_if_applicable,"sv_dbw_low_limit": mandatory_if_applicable,"transmit_duration_nominal_sub_pulse": mandatory_if_applicable,"transmit_lag_interval_sub_pulse": mandatory_if_applicable,"velocity": optional,"velocity_depth_stabilisation": mandatory_if_applicable,"velocity_motion_stabilisation": mandatory_if_applicable,"vertical_sample_interval": mandatory}

    #Sub groups
class MeanCurrentGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return AdcpGrpValidator.get_group_path_pattern()+"Mean_current/"
    _enum_types=[]
    _vlen_types=["ping_t"]

    #Attributes
    _attributes={}

    #Dimensions 
    _dimensions={"mean_time": mandatory}

    #Variables
    _coordinate_variables={"mean_time": mandatory}

    #Variables
    _variables={"averaging": mandatory,"bottom_track_velocity_vessel_x": mandatory_if_applicable,"bottom_track_velocity_vessel_y": mandatory_if_applicable,"bottom_track_velocity_vessel_z": mandatory_if_applicable,"current_velocity_geographical_down": mandatory_if_applicable,"current_velocity_geographical_east": mandatory_if_applicable,"current_velocity_geographical_north": mandatory_if_applicable,"current_velocity_vessel_x": mandatory_if_applicable,"current_velocity_vessel_y": mandatory_if_applicable,"current_velocity_vessel_z": mandatory_if_applicable,"mean_bin_length": mandatory_if_applicable,"mean_platform_heading": mandatory_if_applicable,"mean_platform_latitude": mandatory_if_applicable,"mean_platform_longitude": mandatory_if_applicable,"mean_platform_pitch": mandatory_if_applicable,"mean_platform_roll": mandatory_if_applicable,"mean_platform_vertical": mandatory_if_applicable,"percent_good_limit": mandatory_if_applicable,"ping_averaged": mandatory_if_applicable,"quality": mandatory_if_applicable}
class BathymetryGrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return BeamGroup1GrpValidator.get_group_path_pattern()+"Bathymetry/"
    _enum_types=["detection_type_t","seafloor_backscatter_equation_t"]
    _vlen_types=[]

    #Attributes
    _attributes={"detection_conversion_equation_type": mandatory}

    #Dimensions 
    _dimensions={"detection": mandatory,"seabed_sample": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"detection_latitude": optional,"detection_longitude": optional,"detection_x": mandatory,"detection_y": mandatory,"detection_z": mandatory,"status": mandatory,"status_detail": mandatory,"multiping_sequence": mandatory,"detection_type": mandatory,"detection_beam_stabilisation": optional,"detection_beam_pointing_angle": optional,"detection_two_way_travel_time": optional,"detection_quality_factor": optional,"detection_rx_transducer_index": mandatory,"detection_rx_beam": optional,"detection_tx_beam": mandatory,"detection_tx_transducer_index": mandatory,"detection_backscatter_r": mandatory,"detection_backscatter_i": mandatory,"detection_source_level_applied": mandatory_if_applicable,"detection_receiver_sensitivity_applied": mandatory_if_applicable,"detection_backscatter_calibration": mandatory_if_applicable,"detection_time_varying_gain": mandatory_if_applicable,"detection_mean_absorption_coefficient": mandatory_if_applicable,"seabed_image_start_range": optional,"seabed_image_center": optional,"seabed_image_samples_r": optional}

    #Sub groups
class GridGroup1GrpValidator:
    @staticmethod
    def get_group_path_pattern():
        return SonarGrpValidator.get_group_path_pattern()+"Grid_group[0-9]*"+"/"
    _enum_types=["backscatter_type_t","range_axis_interval_type_t","ping_axis_interval_type_t"]
    _vlen_types=[]

    #Attributes
    _attributes={"beam_mode": mandatory,"conversion_equation_type": mandatory}

    #Dimensions 
    _dimensions={"beam": mandatory,"tx_beam": mandatory,"frequency": mandatory,"ping_axis": mandatory,"range_axis": mandatory}

    #Variables
    _coordinate_variables={}

    #Variables
    _variables={"backscatter_type": mandatory,"beam": mandatory,"beam_reference": mandatory,"beam_stabilisation": mandatory,"beam_type": mandatory,"beamwidth_receive_major": mandatory,"beamwidth_receive_minor": mandatory,"beamwidth_transmit_major": mandatory_if_applicable,"beamwidth_transmit_minor": mandatory_if_applicable,"blanking_interval": mandatory,"cell_depth": mandatory,"cell_latitude": mandatory,"cell_longitude": mandatory,"cell_ping_time": mandatory,"detected_bottom_range": optional,"equivalent_beam_angle": mandatory,"frequency": mandatory,"gain_correction": mandatory_if_applicable,"integrated_backscatter": mandatory,"non_quantitative_processing": mandatory,"ping_axis_interval_type": mandatory_if_applicable,"ping_axis_interval_value": mandatory,"range_axis_interval_type": mandatory,"range_axis_interval_value": mandatory,"receive_duration_effective": mandatory_if_applicable,"receive_transducer_index": mandatory_if_applicable,"receiver_sensitivity": mandatory_if_applicable,"rx_beam_rotation_phi": mandatory,"rx_beam_rotation_psi": mandatory,"rx_beam_rotation_theta": mandatory,"sample_interval": mandatory,"sample_time_offset": mandatory,"sound_speed_at_transducer": optional,"time_varied_gain": mandatory_if_applicable,"transducer_gain": mandatory_if_applicable,"transmit_bandwidth": optional,"transmit_duration_nominal": mandatory,"transmit_frequency_start": mandatory,"transmit_frequency_stop": mandatory,"transmit_power": mandatory_if_applicable,"transmit_source_level": mandatory_if_applicable,"transmit_type": mandatory,"tx_beam_rotation_phi": mandatory,"tx_beam_rotation_psi": mandatory,"tx_beam_rotation_theta": mandatory,"tx_transducer_depth": optional,"waterline_to_chart_datum": optional}
