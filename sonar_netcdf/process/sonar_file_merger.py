import os
from pathlib import Path
from typing import List

import netCDF4 as nc

# if run from sonarnetcdf parent directory we need to disable import errors pylint:disable = import-error
# pylint should be run from root directory as pylint sonarnetcdf
from .. import sonar_groups as sg
from ..utils import nc_merger as nc_m
from ..utils import nc_reader as nc_r
from ..utils import print_color as pc

class SNMerger(nc_m.NCMerger):
    """
    Class used for merging sonar netcdf or XSF files.
    """

    def _extra_check(self, sn_reader: nc_r.NcReader, extra_cheked_obj: str) -> str:
        """
        Check that input file has the expected sonar_type
        """
        sonar_type = _get_sonar_type(sn_reader)
        if extra_cheked_obj is None:
            pc.info(f"Detected sonar_type : '{sonar_type}'")
        elif extra_cheked_obj != sonar_type:
            raise nc_m.SNMergerError("Different sonar_type attribute")
        return sonar_type


def _get_sonar_group(sn_reader: nc_r.NcReader) -> nc.Group:
    return sn_reader.dataset[sg.SonarGrp.get_group_path()]


def _get_sonar_type(sn_reader: nc_r.NcReader) -> str:
    return _get_sonar_group(sn_reader).sonar_type
