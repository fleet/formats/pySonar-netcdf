# -*- coding: utf-8 -*-
"""
This module create all environments defined for the project

"""
import subprocess


def execute(command: str, check: bool = True):
    print(command)
    subprocess.run(command, check=check)


env_name = "sonar_netcdf"

execute(f"conda env remove --name {env_name} -y", check=False)
execute(f"conda env create -f requirements.yml --name  {env_name}")
