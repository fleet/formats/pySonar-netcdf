import os

from IPython.core.display import display
from ipyfilechooser import FileChooser
from ipywidgets import HBox, VBox, Output, Textarea, Button, IntSlider, Checkbox
from ipytree import Tree, Node

import importer  # allow to solve relative imports
import sonar_netcdf.utils.nc_reader as reader
import netCDF4 as nc

from sonar_netcdf.utils.nc_reader_presenter import NcReaderPresenter


class SingleDimensionSelector:
    """Single dimension widget, made of a slider and a check box"""

    def __init__(self, name: str, value: int, max_value: int, enable: bool):
        self.slider = IntSlider(
            description=name, value=value, min=0, max=max_value, disable=False
        )
        self.checkbox = Checkbox(value=enable, description="enable")
        self.widget = HBox([self.slider, self.checkbox])


class DimensionsSelector:
    """Composite (VBox) storing all SingleDimensionSelector widget for one variable"""

    def __init__(self):
        self.widget = VBox([])
        self.clear()
        self.values = {}  # dictionnary use to remind latest dimension values

    def clear(
        self,
    ):
        """Clear widget content"""
        self.widget.children = ()

    def add(self, name, max_value, current_value=None):
        """Add a new dimension and SingleDimensionSelector widget"""
        enable = False
        if current_value is None:
            # try to retrieve a previous dimension with the same name (may be redefined in another group) and to reuse its value
            current_value = 0
            if name in self.values:
                # retrieve the previous widget with the same name
                previous_widget = self.values[name]
                previous_value = previous_widget.slider.value
                # reuse old values for the given dimension
                if previous_value <= max_value:
                    current_value = previous_value
                else:
                    current_value = max(max_value - 1, 0)
                # reuse previous state
                enable = previous_widget.checkbox.value
        # create widget
        selector = SingleDimensionSelector(
            name=name, value=current_value, max_value=max_value, enable=enable
        )
        # add it to the box container
        self.widget.children += (selector.widget,)
        # remember this widget for the dimension
        self.values[name] = selector

    def build_slice_index(self) -> dict:
        dict_index = {
            key: value.slider.value
            for (key, value) in self.values.items()
            if value.checkbox.value is True
        }
        return dict_index


class AppLayout2:
    """Composite handling layout of all widgets"""

    def clear_output(self, event=None):
        self.content_plt.clear_output(wait=False)

    def clear_all(self, event=None):
        """clean up widgets"""
        self.plt_button.disabled = True
        self.plt_button.description = "plot selection"
        self.content_text.value = "Empty content"
        self.content_plt.clear_output(wait=False)

    def __init__(self, top=None):
        self.top = top
        self.content_text = Textarea(
            "Empty content", layout={"width": "100%", "height": "100%"}
        )
        # tree view widget
        self.tree = Tree(multiple_selection=False, layout={"width": "50%"})

        # Main widget
        center = HBox([self.tree, self.content_text], layout={"width": "100%"})

        # create widget for plot area
        self.plt_button = Button(description="plot selection")
        self.plt_button.disabled = True
        clear_button = Button(description="clear")
        self.content_plt = Output(layout={"border": "1px solid black"})
        clear_button.on_click(self.clear_output)
        plt_toolbox = HBox((self.plt_button, clear_button))
        self.plt_dimension_selector = DimensionsSelector()
        plt_widget = VBox(
            [plt_toolbox, self.plt_dimension_selector.widget, self.content_plt]
        )

        # put all widgets in a VBox
        self.widget = VBox([top, center, plt_widget], layout={"width": "100%"})

    def show(self):
        """Display widget"""
        display(self.widget)


class NCExplorer:
    """Create a notebook application allowing to load a netcdf file,
    display in a tree view its groups and variables contents and inspect variables definitions and data
    """

    def __init__(self, starting_path=r"~"):
        # create file chooser
        starting_path = os.path.expanduser(starting_path)
        starting_path = os.path.expandvars(starting_path)
        if not os.path.exists(starting_path):
            new_path = os.path.expanduser("~")
            print(f"Input file {starting_path} does not exist switching to {new_path}")
            starting_path = new_path
        self.fc = self._createFileChooser(starting_path, self.on_file_selected)
        # create full widget
        self.widget = AppLayout2(top=self.fc)

        # retain current selection
        self.current_selection = None  # nc.Variable

        # handle plot event
        self.widget.plt_button.on_click(self.plot_current_variable)

        # if a path is given, initialize with it
        if os.path.isfile(starting_path) and os.path.exists(starting_path):
            self.on_file_selected(starting_path)

    def on_file_selected(self, filename):
        """Handle a new file selection"""
        self.current_file = filename
        print(f"{filename} was selected")
        # clear previous selection
        self.current_selection = None
        # clear tree view
        for n in self.widget.tree.nodes:
            self.widget.tree.remove_node(n)
        # clear link between node and datasets
        self.node_dataset_dict = {}
        # clear all widget
        self.widget.clear_all()

        # open file and set root dataset
        self.current_reader = reader.NcReader(filename)
        root = self.current_reader.dataset

        # init first node
        root_name = os.path.basename(filename)
        root_node = Node(root_name, icon="file", icon_style="info", opened=False)
        self.widget.tree.add_node(root_node)
        root_node.observe(self.handle_tree_click, "selected")

        # create a dictonnary to retains informations between dataset and tree nodes
        self.node_dataset_dict = dict()
        self.node_dataset_dict[root_node] = root

        # recurse all dataset and fill tree
        self._recurseTree(current_group=root, current_node=root_node)

    def show(self):
        """Display the widgets"""
        return self.widget.show()

    def _recurseTree(self, current_group: nc.Dataset, current_node: Node):
        """Recurse data set, and add subgroups/variables in the tree view node as parameter"""
        for g in sorted(current_group.groups):
            n = Node(g, icon="folder", icon_style="info", opened=False)
            current_node.add_node(n)
            self.node_dataset_dict[n] = current_group[g]
            n.observe(self.handle_tree_click, "selected")
            self._recurseTree(current_group.groups[g], n)
        for v in sorted(current_group.variables):
            n = Node(v, icon="leaf", icon_style="info")
            variable = current_group.variables[v]
            n.observe(self.handle_tree_click, "selected")
            self.node_dataset_dict[n] = variable
            current_node.add_node(n)

    def _display_node_overview(self, dataset):
        """
        Compute dump text for the data and update widget
        :param dataset:
        :return:
        """
        marker = "-> "

        if isinstance(dataset, nc.Variable):
            (full_path, name) = reader.NcReader.get_variable_path_and_name(dataset)
            if reader.NcReader.is_variable_vlen(dataset):
                desc = f"Dataset : {full_path} vlen({dataset.dtype})\n\n"
            else:
                desc = f"Dataset : {full_path} ({dataset.dtype})\n\n"
            v = ",".join((f"{dim}" for dim in dataset.dimensions))
            dimensions = f"Dimensions: ({v}) {dataset.shape}"
            desc = "".join([desc, dimensions])
        elif isinstance(dataset, nc.Group):  # NCGroup
            if hasattr(dataset, "path"):
                vpath = dataset.path
            desc = f"Dataset : {vpath}\n\n"
            dimensions = f"Dimensions:\n"
            v = "".join(
                (
                    f"\t{marker}{dim}:{dataset.dimensions[dim].size}\n"
                    for dim in sorted(dataset.dimensions)
                )
            )
            desc = "".join([desc, dimensions, v])
            cmptypes = f"\nCompound types:\n"
            v = "".join(
                (f"\t{marker}{t}:{dataset.cmptypes[t]}\n" for t in dataset.cmptypes)
            )
            desc = "".join([desc, cmptypes, v])

            enum_t = f"\nEnum types:\n"
            v = "".join(
                (
                    f"\t{marker}{t}: type={dataset.enumtypes[t].dtype}, values={dataset.enumtypes[t].enum_dict}\n"
                    for t in dataset.enumtypes
                )
            )
            desc = "".join([desc, enum_t, v])

            vl_type = f"\nVlen types:\n"
            v = "".join(
                (
                    f"\t{marker}{t}: type={dataset.vltypes[t].dtype}\n"
                    for t in dataset.vltypes
                )
            )
            desc = "".join([desc, vl_type, v])
        else:
            # ROOT GRoup
            desc = f"Dataset : / \n\n"

        attributes = f"\nAttributes:\n"
        v = "".join(
            (
                f"\t{marker}{att}:{dataset.getncattr(att)}\n"
                for att in sorted(dataset.ncattrs())
            )
        )
        desc = "".join([desc, attributes, v])

        self.widget.content_text.value = str(desc)

    def _update_current_selection(self, dataset):
        """Selected dataset changed"""
        self.current_selection = dataset
        self._display_node_overview(self.current_selection)

        if self.current_selection is not None and isinstance(
            self.current_selection, nc.Variable
        ):
            # update plot description
            self.widget.plt_button.description = f"Plot  {self.current_selection.name}"
            dimensions = self.current_selection.dimensions
            shape = self.current_selection.shape
            # shape could have one more dimension due to vlen
            # shape = shape[0:len(dimensions)]

            self.widget.plt_dimension_selector.clear()
            for dim, s in zip(dimensions, shape):
                self.widget.plt_dimension_selector.add(
                    name=dim, max_value=s - 1, current_value=None
                )

            self.widget.plt_button.disabled = False
        else:
            self.widget.plt_button.disabled = True

    def handle_tree_click(self, event):
        if event["new"] and event["owner"] is not None:
            node = event["owner"]
            self._update_current_selection(self.node_dataset_dict[node])

    def plot_ncvariable(self, dataset):
        """Plot a variable in widget content_plt context"""
        with self.widget.content_plt:
            path, name = reader.NcReader.get_variable_path_and_name(dataset)
            if path is not None:
                # build a slice index given all dimension selector
                slice_index = self.widget.plt_dimension_selector.build_slice_index()

                presenter = NcReaderPresenter(self.current_reader)
                presenter._display_variable(
                    variable_name=name, variable_path=path, slice_index=slice_index
                )

    def plot_current_variable(self, event):
        """Retrieve current variable and plot it"""
        if self.current_selection is not None and isinstance(
            self.current_selection, nc.Variable
        ):
            self.plot_ncvariable(self.current_selection)

    def _createFileChooser(self, path: str, callback):
        """Create file chooser"""
        if not os.path.isfile(path):
            fc = FileChooser(path)
        else:
            fc = FileChooser(filename=path)
            # Create and display a FileChooser widget

        # create our own callback that just call by passing the selected file
        def on_selection(chooser):
            callback(chooser.selected)

        fc.register_callback(on_selection)

        return fc


"""Code only used for debug purpose, should be started in a notebook"""
if __name__ == "__main__":
    import matplotlib.pyplot as plt

    d = r"C:\data\datasets\XSF\data\0006_20200504_111056_FG_EM122.xsf.nc"
    f = NCExplorer(d)

    f.on_file_selected(d)
    f._display_node_overview(
        f.current_reader.dataset["/Sonar/Beam_group1/backscatter_r"]
    )
    f._update_current_selection(
        f.current_reader.dataset["/Sonar/Beam_group1/backscatter_r"]
    )
    f.plot_ncvariable(f.current_reader.dataset["/Sonar/Beam_group1/platform_longitude"])
    plt.show()
